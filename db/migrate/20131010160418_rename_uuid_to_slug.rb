class RenameUuidToSlug < ActiveRecord::Migration
  def change
    rename_column :items, :uuid, :slug
    rename_column :gs_users, :uuid, :slug
  end
end
