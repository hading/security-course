class LoginDataValidator < ActiveModel::Validator
  def validate(record)
    unless record.user_name.match(/\w{4,30}/)
      record.errors[:user_name] = ('invalid user name')
    end
  end
end