class CreateGsUsers < ActiveRecord::Migration
  def change
    create_table :gs_users do |t|
      t.string :user_name
      t.string :password
      t.string :user_type
    end
  end
end
