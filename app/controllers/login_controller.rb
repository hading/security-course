class LoginController < ApplicationController

  def login

  end

  def fail

  end

  def success

  end

  def submit_login
    #real code
    user = GsUser.where(:user_name => params[:login][:user_name], :password => params[:login][:password]).first
    #direct sql code
    #here we can get into the success branch, or execute arbitrahttp://slashdot.org/ry sql, e.g. by using user_name = "' ; my sql code here -- '"
    #login_data = LoginData.new(params[:login])
    #valid = login_data.valid?
    # user = ActiveRecord::Base.connection.send(:select, ("SELECT * from gs_users where user_name='#{params[:login][:user_name]}' AND password = '#{params[:login][:password]}'")).rows.present?
    if user
      session[:user_id] = user.id
      redirect_to items_path
    else
      render 'fail'
    end
  end
end