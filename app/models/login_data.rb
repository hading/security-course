class LoginData
  include ActiveModel::Validations
  validates_with LoginDataValidator

  attr_accessor :user_name, :password

  def initialize(attributes = {})
    self.user_name = attributes[:user_name]
    self.password = attributes[:password]
  end

end