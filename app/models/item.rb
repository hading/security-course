class Item < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug, use: :slugged

  before_save :sanitize_description
  before_create :assign_slug

  def sanitize_description
    self.description = Sanitize.clean(self.description, Sanitize::Config::BASIC)
  end

  def assign_slug
    self.slug = UUID.generate
  end

end