class GsUser < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug, use: :slugged

  validates_uniqueness_of :user_name, :allow_blank => false
  validates_presence_of :password
  has_many :items, :foreign_key => 'user_id', :order => 'description asc'

  before_create :assign_slug

  def assign_slug
    self.slug = UUID.generate
  end
end