class ConnectionFactory

  def self.get_connection
    return ActiveRecord::Base.connection
  end

  def get_connection
    self.class.get_connection
  end

  def self.initialize_test_data
    GsUser.all.each { |user| user.destroy }
    Item.all.each { |item| item.destroy }
    [%w(Jerry comic seller), %w(Cosmo kramer seller),
     %w(Elaine witchy buyer), %w(George bosco buyer)].each do |name, pw, type|
      GsUser.create(:user_name => name, :password => pw, :user_type => type)
    end
    [['Jerry', 'Puffy Shirt - Look like a <b>pirate<\b>', 18.00],
     ['Jerry', 'Stinky car', 18000.00],
     ['Cosmo', 'Coffee table book', 16.00],
     ['Cosmo', 'Calvin Klein Underwear', 8.00]].each do |name, description, price|
      Item.create(:user_id => GsUser.find_by_name(name).id, :description => description,
                  :price => price)
    end
  end
end