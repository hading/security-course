class AddUuidToItems < ActiveRecord::Migration
  def change
    add_column :items, :uuid, :string, :unique => true
    add_index :items, :uuid
    Item.all.each do |item|
      item.uuid = UUID.generate
      item.save!
    end
  end
end
