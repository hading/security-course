class ItemsController  < ApplicationController

  before_filter :find_item, only: [:edit, :update, :show]
  before_filter :verify_owner, only: [:edit, :update]

  #display items for logged in user only
  def index
    redirect_to login_path unless session[:user_id].present?
    @user = GsUser.find(session[:user_id])
    @items = @user.items
  end

  def edit

  end

  def update
    if @item.update_attributes(params.require(:item).permit(:description, :price))
      redirect_to items_path
    else
      render 'edit'
    end
  end

  def show

  end

  private

  def find_item
    @item = Item.friendly.find(params[:id])
  end

  def verify_owner
    redirect_to items_path unless @item.user_id == session[:user_id]
  end
end