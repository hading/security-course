class AddUuidToUsers < ActiveRecord::Migration
  def change
    add_column :gs_users, :uuid, :string, :unique => true
    add_index :gs_users, :uuid
    GsUser.all.each do |user|
      user.uuid = UUID.generate
      user.save!
    end
  end
end
